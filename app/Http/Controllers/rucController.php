<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class rucController extends Controller
{
    
    public function store(Request $request ){
        // return $request dni; 
        $dni = $request->dni;
        $client = new Client([
            "base_uri"=>"https://webservice.rbl.com.ve/web_service/api/v1/"
        ]);
        $response = $client->post("get_dni?token=app4000&dni=$dni");
        $muestra= ($response->getBody()->getContents());
        print_r(json_decode((string) $muestra));
        
        return view('respuesta', compact('muestra'));  
    }

    public function store2(Request $request ){
        // return $request ruc; 
        $ruc = $request->ruc;
        $client = new Client([
            "base_uri"=>"https://webservice.rbl.com.ve/web_service/api/v1/"
        ]);
        $response = $client->post("get_ruc?token=app4000&ruc_consultar=$ruc");
        $muestra = $response->getBody()->getContents();
        $obj=json_decode($muestra,FALSE);
       
        return view('respuestaruc',compact('obj'));  

    }
}

