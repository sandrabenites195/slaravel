<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class postController extends Controller
{
    //
    public function index(){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://jsonplaceholder.typicode.com',
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);
        
        // $array = [
        //     "token" => "app4000",
        //     "ruc_consultar" => "10752477383",
        // ];

        $response = $client->request('GET', 'posts'); //post only  with params  dimanics
        
        $posts=json_decode($response->getBody()->getContents());
        
        return view('posts.index', compact('posts'));
        
      
    }

    public function show($id){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://jsonplaceholder.typicode.com',
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);
        
        
        $response = $client->request('GET', "posts/{$id}");
        
        $post=json_decode($response->getBody()->getContents());
        
        return view('posts.show', compact('post'));

    }

    public function gesan(){

        $token='app4000';
        $dni='44174114';
        $client = new Client([
            'base_uri' => 'https://webservice.rbl.com.ve/web_service/api/v1/get_dni?token='.$token.'&dni='.$dni,
        ]);
      
        $response = $client->request('POST');  //post only  with params  dimanics
        
        $cuerpo=$response->getBody();
        print_r(json_decode((string) $cuerpo));
        
        return view('sostic', compact('cuerpo'));
    }

    public function consumo_formulario(Request $request){
        
        $client = new Client([
            'base_uri' => 'https://webservice.rbl.com.ve/web_service/api/v1/get_dni?token=app4000&',
        ]);
        //cuando se ejecute esta linea la url sera , miapi.com/obtenerdni
        $response = $client->post('dni',[
            "json"=>["dni"=>$request->dni]
        ]);
        $muestra=$response->getBody();
        print_r(json_decode((string) $muestra));
        return view('dniv', compact('muestra'));
    }
}
