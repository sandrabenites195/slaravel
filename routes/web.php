<?php

use Illuminate\Support\Facades\Route;


use function GuzzleHttp\json_decode;


// consumo  con guzzz

Route::get('/','postController@index');
Route::get('/posts/{id}','postController@show');
Route::get('/a','postController@gesan');
// Route::get('/b','postController@consumo_formulario');
 //esta es la vista 

//  return view('posts.index', compact('posts'));

// Route::post('formulario','rucController@store')->name('fm'); 
Route::view('/formulario', 'formulario')->name('fm');  //vista del  formulario dni 
Route::post('respuesta', 'rucController@store')->name('respuesta'); //vista respuesta dni 

Route::view('/fruc', 'fruc')->name('fruc');  //vista del  formulario ruc
Route::post('respuestaruc', 'rucController@store2')->name('respuestaruc'); //vista respuesta ruc

Route::view('/consulta', 'consulta')->name('consulta');  //vista consulta  
// Route::post('respuesta', 'rucController@store')->name('respuesta'); //vista respuesta consulta

//rutas del pdf 
Route::get('/visionpdf','PDFController@PDF')->name('descargarPDF');