<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>consulta</title>
</head>
<body>
        <h1 class="text-center">vista consulta</h1>
    <form action="" class="text-center">
        <div class="form-row ">
            <div class="col-lg-4 control-label">Ingrese Datos</div>
        </div>
        <div class="row">
            <div class="col">
                <div class="label">tipo de documento</div>
            </div>
            <div class="col">
                <select id="inputState" class="form-control form-control-lg-2">
                    <option selected>Escoja una opcion</option>
                    <option>Factura</option>
                    <option>Boleta</option>
                  </select>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="label">Serie o Numero</div>
            </div>
            <div class="col">
               <input type="text" name="" id="" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="label">Ruc del emisor</div>
            </div>
            <div class="col">
               <input type="number" name="" id="" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="label">Importe total</div>
            </div>
            <div class="col">
               <input type="number" name="" id="" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="label">Fecha de Emision</div>
            </div>
            <div class="col">
                <input type="date" name="" step="1" min="1990-01-01" max="2020-01-31" class="" value="<?php echo date("Y-m-d");?>">
            </div>
        </div>
    </form>
</body>
</html>