<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <title>blog</title>
</head>
<body>
  <div class="container">
    <h1>Respuestas de tipo post</h1>

        <div class="panel panel-default">
          <div class="panel-header"> //heading
               {{$post ->title}}
          </div>
            <div class="panel-body">
              {{$post ->body}}
            </div>
        </div>
   </div>
    
  
</body>
</html>